package com.leewise.studycafemanage.controller;

import com.leewise.studycafemanage.model.CafeCustomerItem;
import com.leewise.studycafemanage.model.CafeCustomerRequest;
import com.leewise.studycafemanage.model.CustomerTimeUpdateRequest;
import com.leewise.studycafemanage.service.CafeManageService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/cafe-manage")
public class CafeManageController {
    private final CafeManageService cafeManageService;
    @PostMapping("/customer")
    public String setCustomerService(@RequestBody CafeCustomerRequest request){
        cafeManageService.setCustomerManage(request);
        return "Thanks, Now You can Use Our Service";
    }
    @GetMapping("/data")
    public List<CafeCustomerItem> getCustomerInfo() {
        List<CafeCustomerItem> result = cafeManageService.getCustomerData();

        return result;
    }
    @PutMapping("/add-time/id/{id}")
    public String putCustomerTime(@PathVariable long id, @RequestBody @Valid CustomerTimeUpdateRequest request){
        cafeManageService.putCustomerTime(id, request);

        return "thanks We Updated";
    }
    @PutMapping("/change-time/id/{id}")
    public String putCustomerOut(@PathVariable long id) {
        cafeManageService.putCustomerTimeOut(id);
        return "thanks Come Again!";
    }
    @DeleteMapping("/sign-out/id/{id}")
    public String delCustomer(@PathVariable long id) {
        cafeManageService.delCustomer(id);
        return "thanks !";
    }
}
