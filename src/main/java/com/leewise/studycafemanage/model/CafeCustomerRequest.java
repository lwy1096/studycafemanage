package com.leewise.studycafemanage.model;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class CafeCustomerRequest {
    @NotNull
    @Length(min=2,max = 10)
    private String name;

    @NotNull
    @Length(min=11,max = 20)
    private String phoneNumber;

    @NotNull
    private Integer tableNumber;

    @NotNull
    @Min(value = 1)
    @Max(value = 12)
    private Integer usedHour;


    private Integer TotalHours;
    private LocalDateTime enterTime;
    private LocalDateTime leftTime;
}
