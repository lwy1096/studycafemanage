package com.leewise.studycafemanage.repository;

import com.leewise.studycafemanage.entity.CafeCustomer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerManageRepository extends JpaRepository<CafeCustomer,Long> {
}
