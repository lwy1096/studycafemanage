package com.leewise.studycafemanage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudyCafeManageApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudyCafeManageApplication.class, args);
    }

}
