package com.leewise.studycafemanage.service;

import com.leewise.studycafemanage.entity.CafeCustomer;
import com.leewise.studycafemanage.model.CafeCustomerItem;
import com.leewise.studycafemanage.model.CafeCustomerRequest;
import com.leewise.studycafemanage.model.CustomerTimeUpdateRequest;
import com.leewise.studycafemanage.repository.CustomerManageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CafeManageService {
    private final CustomerManageRepository customerManageRepository;
    public void setCustomerManage(CafeCustomerRequest request) {
        CafeCustomer addData = new CafeCustomer();
        addData.setName(request.getName());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setTableNumber(request.getTableNumber());
        addData.setUsedHour(request.getUsedHour());
        addData.setEnterTime(LocalDateTime.now());
        addData.setLeftTime(request.getUsedHour());
        addData.setTotalHours(request.getUsedHour());
        customerManageRepository.save(addData);

    }
    public List<CafeCustomerItem> getCustomerData(){
        List<CafeCustomer> originData = customerManageRepository.findAll();
        List<CafeCustomerItem> result = new LinkedList<>();
        for(CafeCustomer item : originData){
            CafeCustomerItem addItem = new CafeCustomerItem();
            addItem.setId(item.getId());
            addItem.setName(item.getName());
            addItem.setPhoneNumber(item.getPhoneNumber());
            addItem.setTableNumber(item.getTableNumber());
            addItem.setUsedHour(item.getUsedHour());
            addItem.setEnterTime(item.getEnterTime());
            addItem.setLeftTime(item.getTotalHours() - item.getUsedHour());
            addItem.setLastEnterTime(item.getLastEnterTime());
            addItem.setTotalHours( item.getTotalHours());
            result.add(addItem);
        }
        return result;
    }
    public void putCustomerTime(long id, CustomerTimeUpdateRequest request){
        CafeCustomer originData = customerManageRepository.findById(id).orElseThrow();
        originData.setUsedHour(request.getUsedHour()+originData.getUsedHour());
        originData.setLeftTime(originData.getTotalHours() - originData.getUsedHour() );
        originData.setTotalHours((originData.getUsedHour()>0? originData.getUsedHour()+request.getUsedHour():request.getUsedHour() ));
        customerManageRepository.save(originData);
    }
    public void putCustomerTimeOut(long id){
        CafeCustomer originData = customerManageRepository.findById(id).orElseThrow();
        originData.setLastEnterTime(LocalDateTime.now());
        customerManageRepository.save(originData);
    }
    public void delCustomer(long id){
        customerManageRepository.deleteById(id);
    }
}
