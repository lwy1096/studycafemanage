package com.leewise.studycafemanage.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class CafeCustomer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String name;
    @Column(nullable = false, length = 20)
    private String phoneNumber;
    @Column(nullable = false )
    private Integer usedHour;

    @Column(nullable = false )
    private Integer tableNumber;
    private Integer leftTime;
    private LocalDateTime enterTime;
    private LocalDateTime lastEnterTime;

    private Integer TotalHours;


}
