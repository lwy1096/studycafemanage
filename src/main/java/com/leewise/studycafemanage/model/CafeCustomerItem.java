package com.leewise.studycafemanage.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class CafeCustomerItem {

    private Long id;
    private String name;

    private String phoneNumber;

    private Integer tableNumber;

    private Integer usedHour;

    private LocalDateTime enterTime;

    private Integer leftTime;
    private LocalDateTime lastEnterTime;

    private Integer totalHours;
}
