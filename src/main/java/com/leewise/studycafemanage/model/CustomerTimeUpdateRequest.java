package com.leewise.studycafemanage.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class CustomerTimeUpdateRequest {

    private Integer addTime;

    private Integer usedHour;
    private LocalDateTime enterTime;
    private Integer totalHours;
}
